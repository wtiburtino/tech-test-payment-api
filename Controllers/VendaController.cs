using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class VendaController : ControllerBase
    {
        private readonly OrganizadorContext _context;

        public VendaController(OrganizadorContext context)
        {
            _context = context;
        }
        
        [HttpGet("{id}")]
        public IActionResult BuscarPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
                return NotFound();

            var itens = _context.ItemDeVenda.Where(x => x.VendaId == id);
            foreach(ItemDeVenda item in itens)
            {
                venda.adicionarItemDeVenda(item);
            }

            var vendedor = _context.Vendedor.Find(venda.VendedorID);

            return Ok(venda);
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            //if (!venda.Data.HasValue)
            //    return BadRequest(new { Erro = "A data da venda não pode ser nula." });

            if (venda.Data == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da venda não pode ser vazia." });

            if (venda.ItemDeVenda.Count < 1)
                return BadRequest(new { Erro = "A lista de itens de venda não pode ser vazia." });

            if (venda.Status != EnumStatusVenda.AguardandoPagamento)
                return BadRequest(new { Erro = $"A venda deve ser registrada com status {EnumStatusVenda.AguardandoPagamento}." });

            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(BuscarPorId), new { id = venda.Id }, venda);
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarVenda(int id, Venda venda)
        {
            var vendaBD = _context.Vendas.Find(id);

            if (vendaBD == null)
                return NotFound();

    
            switch(vendaBD.Status)
            {
                case EnumStatusVenda.AguardandoPagamento:
                    if (venda.Status != EnumStatusVenda.PagamentoAprovado && 
                            venda.Status != EnumStatusVenda.Cancelada)
                        return BadRequest(new { Erro = $"O status {vendaBD.Status} não pode ser sucedido pelo status {venda.Status}." });
                    break;

                case EnumStatusVenda.PagamentoAprovado:
                    if (venda.Status != EnumStatusVenda.EnviadoParaTransportadora &&
                            venda.Status != EnumStatusVenda.Cancelada)
                        return BadRequest(new { Erro = $"O status {vendaBD.Status} não pode ser sucedido pelo status {venda.Status}." });
                    break;
                case EnumStatusVenda.EnviadoParaTransportadora:
                    if (venda.Status != EnumStatusVenda.Entregue)
                        return BadRequest(new { Erro = $"O status {vendaBD.Status} não pode ser sucedido pelo status {venda.Status}." });
                    break;
            }

            vendaBD.Status = venda.Status;

            _context.Vendas.Update(vendaBD);
            _context.SaveChanges();
            
            return CreatedAtAction(nameof(BuscarPorId), new { id = vendaBD.Id }, vendaBD);
        }
    }
}
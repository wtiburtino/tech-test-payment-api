using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly OrganizadorContext _context;

        public VendedorController(OrganizadorContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult BuscarPorId(int id)
        {
            var vendedor = _context.Vendedor.Find(id);
            if (vendedor == null)
                return NotFound();
            return Ok(vendedor);
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            return CreatedAtAction(nameof(BuscarPorId), new { id = vendedor.Id }, vendedor);
        }
    }
}
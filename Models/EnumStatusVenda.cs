using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models
{
    public enum EnumStatusVenda
    {
        [Display(Name = "Aguardando Pagamento")]
        AguardandoPagamento,
        [Display(Name = "Pagamento Aprovado")]
        PagamentoAprovado,
        [Display(Name = "Enviado para Transportadora")]
        EnviadoParaTransportadora,
        Entregue,
        Cancelada
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        private int _id;
        private DateTime? _data;
        private EnumStatusVenda _status;
        private ICollection<ItemDeVenda> _itemDeVenda;
        private int _vendedorID;
        private Vendedor _vendedor;

        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public DateTime? Data 
        { 
            get => _data;
            set => _data = value;
        }

        public EnumStatusVenda Status
        {
            get => _status;
            set => _status = value;
        }

        public virtual ICollection<ItemDeVenda> ItemDeVenda 
        { 
            get => _itemDeVenda;
            set => _itemDeVenda = value; }
        
        [ForeignKey("VendedorId")]
        public int VendedorID 
        { 
            get => _vendedorID;
            set => _vendedorID = value; }
        public virtual Vendedor Vendedor 
        { 
            get => _vendedor;
            set => _vendedor = value; 
        }

        public Venda() {}

        public Venda(ICollection<ItemDeVenda> itemDeVenda , int vendedorID)
        {
            ItemDeVenda = itemDeVenda;
            VendedorID = vendedorID;
            Status = EnumStatusVenda.AguardandoPagamento;
        }

        public Venda (int vendedorID)
        {
            VendedorID = vendedorID;
            Status = EnumStatusVenda.AguardandoPagamento;
        }

        public void adicionarItemDeVenda(ItemDeVenda itemDeVenda)
        {
            this._itemDeVenda.Add(itemDeVenda);
        }
    }
}